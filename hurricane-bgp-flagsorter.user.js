// ==UserScript==
// @name        Hurricane BGP Flagsorter
// @namespace   http://www.darkdevil.dk
// @require	https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js
// @include     *://bgp.he.net/AS*
// @version     1.0.0
// @grant       none
// @downloadURL https://bitbucket.org/DarkServ/hurricane-bgp-flagsorter/raw/master/hurricane-bgp-flagsorter.user.js
// @updateURL   https://bitbucket.org/DarkServ/hurricane-bgp-flagsorter/raw/master/hurricane-bgp-flagsorter.user.js
// ==/UserScript==

(function () {
  "use strict";
  var prefixes_v4_head = $('#table_prefixes4 > thead');
  prefixes_v4_head.html('<tr><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Prefix<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Description<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">CC<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th></tr>');
  var prefixes_v6_head = $('#table_prefixes6 > thead');
  prefixes_v6_head.html('<tr><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Prefix<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Description<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">CC<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th></tr>');
  var peers_v4_head = $('#table_peers4 > thead');
  peers_v4_head.html('<tr><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Rank<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Description<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">CC<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">IPv6<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Peer<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th></tr>');
  var peers_v6_head = $('#table_peers6 > thead');
  peers_v6_head.html('<tr><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Rank<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Description<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">CC<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">IPv4<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th><th><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">Peer<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th></tr>');
  var flag_re = '/.*\/flags\/(.*?)\.gif.*/';
  $('.flag').each(function() {
    if ($(this).parent().attr("class") != 'asright') {
      var img = $('> img', this);
      if (img) {
        img.attr("height", "20");
        img.attr("width", "30");
      }
      var img_src = img.attr("src");
      if (typeof img_src  !== 'undefined') {
        var flag = img_src.match(/\/flags\/(.*?)\.gif/)[1];
      } else {
        console.log('img_src: undefined');
        var flag = '';
      }
      $(this).parent().after('<td>' + flag + '</td>');
    }
  });
}(jQuery.noConflict(true)));
